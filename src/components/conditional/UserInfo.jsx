import If from "./If"


export default props => {
  const user = props.user || {}
  return (
    <div>
      <If test={user && user.name}>
        Seja bem-vindo <strong>{props.user.name}</strong>!
      </If>
      <If test={!user || !user.name}>
        Seja bem-vindo <strong>Amigão!</strong>
      </If>
      
    </div>
  )
}