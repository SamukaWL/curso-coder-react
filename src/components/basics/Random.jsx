

export default props => {
  
  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  return (
    <>
      <h4>Número max é {props.max}</h4>
      <h4>Número min é {props.min}</h4>
      <p>Gerando um número aleatório - {getRandomInt(props.min, props.max)}</p>
    </>
  );
}