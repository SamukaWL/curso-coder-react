export default function WithProps(props) {
  const { title, subtitle } = props;
  return (
    <div>
      <h3>{title}</h3>
      <p>{subtitle}</p>
    </div>
  );
}