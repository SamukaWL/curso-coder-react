export default function First() {
  const msg = 'Seja bem-vindo(a)!'
  return (
    <div>
      <h3>Primeiro Componente</h3>
      <p>{ msg }</p>
    </div>
  );
}