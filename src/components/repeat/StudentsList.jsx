import students from '../../dump/students';

export default (props) => {
  return (
    <div>
      <ul>
      {
        students.map(student => {
          return <li key={student.id}>{student.name} - {student.grade}</li>
        })
      }
      </ul>
    </div>
  )
}