import Family from './components/basics/Family';
import FamilyMember from './components/basics/FamilyMember';
import First from './components/basics/First';
import Random from './components/basics/Random';
import WithProps from './components/basics/WithProps';
import UserInfo from './components/conditional/UserInfo';
import Card from './components/layout/Card';
import StudentsList from './components/repeat/StudentsList';

export default () => {
  return (
    <div>
      <h1>Fundamentos React(arrow)</h1>

      <Card>
        <UserInfo user={{name: "Samuel"}} />
        <UserInfo user={{email: "Samuel@mail.com"}} />

      </Card>

      <Card title="Repitição">
        <StudentsList/>
      </Card>

      <Card title="Componente com filhos">
        <Family lastname="Lorena">
          <FamilyMember name="Pedro"></FamilyMember>
          <FamilyMember name="Ana"></FamilyMember>
          <FamilyMember name="Gustavo"></FamilyMember>
        </Family>
      </Card>

      <Card title="Desafio aleatório">
        <Random max="10" min="1" />
      </Card>

      <Card title="Primeiro componente">
        <First />
      </Card>

      <Card title="Com parâmetro">
        <WithProps
          title="Segundo componente"
          subtitle="Muito legal!"
        />
      </Card>
    </div>
  );
}