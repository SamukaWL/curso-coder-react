export default [
  {id: 1, name: 'Ana', grade: 9.2},
  {id: 2, name: 'Bia', grade: 7.5},
  {id: 3, name: 'Carlos', grade: 6.2},
  {id: 4, name: 'Gui', grade: 10.0},
  {id: 5, name: 'Oliver', grade: 5.5},
  {id: 6, name: 'Thomas', grade: 2.3},
]