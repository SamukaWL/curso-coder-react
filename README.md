# Entendo o basico do react
# React.js

O react te da o poder de construir um “HTML” personalizado para sua aplicação utilizando JS.

```
<img src=”” alt=””/> // html
<usuario item=”” /> // react
```
Perceba a semelhança, com o react podemos simular o mesmo comportamento do HTML, “_componentizando”,_ porém podemos manipular o que acontece por de traz dos panos. Manipulando valores, estados, etc. 

# O que é?

Uma biblioteca JavaScript para criar interfaces de usuário

# Que problema resolve?

1. Estático; 
2. SPA;
3. Dinâmico;


# Props

Como o nome diz são propriedades que são passadas para o componente, muito parecido com o HTML, porém essa propriedades por baixo dos panos é basicamente um JSON dinâmico nomeado props. Que pode ser acessado no componente.

```
Código
<Aluno ra="DD2654185" nome="Luan" />

function Aluno(props){
  return (
    <h1>O {props.nome} e dono do ra {props.ra}</h1>
  )
}

Por baixo dos panos

props: {
  aluno: "DD2654185",
  nome: "Luan"
}
```
# Children
# State


